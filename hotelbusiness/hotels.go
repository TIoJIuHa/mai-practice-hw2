//go:build !solution

package hotelbusiness

type Guest struct {
	CheckInDate  int
	CheckOutDate int
}

type Load struct {
	StartDate  int
	GuestCount int
}

func ComputeLoad(guests []Guest) []Load {
	var data Load
	var output []Load
	var last_date int
	guests_in := make(map[int]int)
	guests_out := make(map[int]int)
	result := make(map[int]int)

	for _, guest := range guests {
		guests_in[guest.CheckInDate] += 1
		guests_out[guest.CheckOutDate] += 1

		if guest.CheckOutDate > last_date {
			last_date = guest.CheckOutDate
		}
	}

	for key, value := range guests_in {
		result[key] += value
	}

	for key, value := range guests_out {
		result[key] -= value
	}

	for i := 1; i <= last_date; i++ {
		result[i] += result[i-1]
	}
	if result[0] != 0 {
		data.GuestCount = result[0]
		data.StartDate = 0
		output = append(output, data)
	}

	for i := 1; i <= last_date; i++ {
		if result[i-1] != result[i] {
			data.GuestCount = result[i]
			data.StartDate = i
			output = append(output, data)
		}
	}

	return output
}
