//go:build !solution

package main

import (
	"errors"
	"strconv"
	"strings"
)

type Evaluator struct {
	commands map[string][]string
}

// NewEvaluator creates evaluator.
func NewEvaluator() *Evaluator {
	return &Evaluator{commands: map[string][]string{
		"dup":  {"dup"},
		"over": {"over"},
		"drop": {"drop"},
		"swap": {"swap"},
		"+":    {"+"},
		"-":    {"-"},
		"*":    {"*"},
		"/":    {"/"},
	},
	}
}

func (e *Evaluator) ExecuteCommand(command string, stack []int) ([]int, error) {
	switch command {
	case "dup":
		if len(stack) < 1 {
			return stack, errors.New("Dup: Not enough arguments!")
		}
		stack = append(stack, stack[len(stack)-1])
	case "over":
		if len(stack) < 2 {
			return stack, errors.New("Over: Not enough arguments!")
		}
		stack = append(stack, stack[len(stack)-2])
	case "drop":
		if len(stack) < 1 {
			return stack, errors.New("Drop: Not enough arguments!")
		}
		stack = stack[0 : len(stack)-1]
	case "swap":
		if len(stack) < 2 {
			return stack, errors.New("Swap: Not enough arguments!")
		}
		stack[len(stack)-1], stack[len(stack)-2] = stack[len(stack)-2], stack[len(stack)-1]
	case "+":
		if len(stack) < 2 {
			return stack, errors.New("Sum: Not enough arguments!")
		}
		sum := stack[len(stack)-2] + stack[len(stack)-1]
		stack = stack[0 : len(stack)-2]
		stack = append(stack, sum)
	case "-":
		if len(stack) < 2 {
			return stack, errors.New("Sub: Not enough arguments!")
		}
		sub := stack[len(stack)-2] - stack[len(stack)-1]
		stack = stack[0 : len(stack)-2]
		stack = append(stack, sub)
	case "*":
		if len(stack) < 2 {
			return stack, errors.New("Mul: Not enough arguments!")
		}
		mul := stack[len(stack)-2] * stack[len(stack)-1]
		stack = stack[0 : len(stack)-2]
		stack = append(stack, mul)
	case "/":
		if len(stack) < 2 {
			return stack, errors.New("Div: Not enough arguments!")
		}
		if stack[len(stack)-1] == 0 {
			return stack, errors.New("Div: Integer divide by zero!")
		}
		div := stack[len(stack)-2] / stack[len(stack)-1]
		stack = stack[0 : len(stack)-2]
		stack = append(stack, div)
	default:
		if num, err := strconv.Atoi(command); err == nil {
			stack = append(stack, num)
		} else {
			return stack, errors.New("Wrong command!")
		}
	}
	return stack, nil
}

// Process evaluates sequence of words or definition.
//
// Returns resulting stack state and an error.
func (e *Evaluator) Process(row string) ([]int, error) {
	var stack []int
	var err error
	actions := strings.Split(row, " ")

	if actions[0] == ":" {
		if len(actions) < 4 {
			return stack, errors.New("User command: Not enough arguments!")
		}

		if _, err := strconv.Atoi(actions[1]); err == nil {
			return stack, errors.New("User command: Can`t redefine numbers!")
		}

		var temp_list []string

		for i := 2; actions[i] != ";"; i++ {
			action := strings.ToLower(actions[i])

			if _, err := strconv.Atoi(action); err != nil && e.commands[action] == nil {
				return stack, errors.New("User command: Wrong command!")
			}

			if e.commands[action] != nil {
				temp_list = append(temp_list, e.commands[action]...)
			} else {
				temp_list = append(temp_list, action)
			}

			e.commands[strings.ToLower(actions[1])] = temp_list
		}
	} else {
		for _, elem := range actions {
			action := strings.ToLower(elem)
			if _, err := strconv.Atoi(action); err == nil {
				if stack, err = e.ExecuteCommand(action, stack); err != nil {
					return stack, err
				}
			} else {
				if e.commands[action] == nil {
					return stack, errors.New("Wrong command!")
				}

				for _, command := range e.commands[action] {
					if stack, err = e.ExecuteCommand(command, stack); err != nil {
						return stack, err
					}
				}
			}
		}
	}
	return stack, err
}
