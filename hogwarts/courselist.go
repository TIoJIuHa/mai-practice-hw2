//go:build !solution

package hogwarts

func CheckCourse(course string, prereqs map[string][]string, check map[string]bool, learned map[string]bool, course_list []string) []string {
	if check[course] {
		panic("Looping!")
	}

	if learned[course] {
		return course_list
	}

	check[course] = true

	for _, prereq := range prereqs[course] {
		course_list = CheckCourse(prereq, prereqs, check, learned, course_list)
	}

	learned[course] = true
	check[course] = false
	course_list = append(course_list, course)

	return course_list
}

func GetCourseList(prereqs map[string][]string) []string {
	var result []string
	check := make(map[string]bool)
	learned := make(map[string]bool)

	for course := range prereqs {
		result = CheckCourse(course, prereqs, check, learned, result)
	}

	return result
}
