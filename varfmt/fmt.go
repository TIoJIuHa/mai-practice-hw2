//go:build !solution

package varfmt

import (
	"fmt"
	"strconv"
	"strings"
)

func Sprintf(format string, args ...interface{}) string {
	var flag bool
	var num strings.Builder
	var result_string strings.Builder
	count := -1

	for _, f := range format {
		if flag {
			if f != '}' {
				num.WriteRune(f)
			} else {
				if num.Len() == 0 {
					result_string.WriteString(fmt.Sprint(args[count]))
				} else if number, err := strconv.Atoi(num.String()); err == nil {
					result_string.WriteString(fmt.Sprint(args[number]))
					num.Reset()
				}
				flag = false
			}
		} else {
			if f != '{' && f != '}' {
				result_string.WriteRune(f)
			}
		}

		if f == '{' {
			flag = true
			count += 1
		}
	}

	return result_string.String()
}
