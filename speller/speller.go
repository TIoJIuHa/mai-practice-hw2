//go:build !solution

package speller

import "strings"

var (
	numbers       = []string{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}
	teens         = []string{"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"}
	dozens        = []string{"", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"}
	large_numbers = []string{"", "thousand", "million", "billion", "trillion"}
)

func SpellPart(x int64, count int) string {
	var res []string
	rem := x % 100
	x /= 100

	if x != 0 {
		res = append(res, numbers[x], "hundred")
	}

	if rem > 10 && rem < 20 {
		res = append(res, teens[rem-10])
	} else {
		var dosen string
		dosen += dozens[rem/10]
		if rem%10 != 0 {
			if rem/10 != 0 {
				dosen += "-"
			}
			dosen += numbers[rem%10]
		}
		res = append(res, dosen)
	}

	if count > 1 {
		res = append(res, large_numbers[count-1])
	}

	return strings.Join(res, " ")
}

func Spell(n int64) string {
	var spell_number string
	number_parts := []string{}

	if n < 0 {
		return "minus " + Spell(-n)
	}

	if n >= 0 && n < 10 {
		return numbers[n]
	}

	if n >= 10 && n < 20 {
		return teens[n-10]
	}

	var remainder int64
	count := 0
	num := n

	for num > 0 {
		remainder = num % 1000
		num /= 1000
		count += 1

		if remainder != 0 {
			number_parts = append(number_parts, SpellPart(remainder, count))

			if num > 0 {
				number_parts = append(number_parts, " ")
			}
		}
	}

	for i := len(number_parts) - 1; i >= 0; i-- {
		spell_number += number_parts[i]
	}

	if spell_number[len(spell_number)-1] == ' ' {
		return spell_number[:len(spell_number)-1]
	}

	return spell_number
}
