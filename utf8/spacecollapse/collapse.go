//go:build !solution

package spacecollapse

import (
	"strings"
	"unicode"
)

func CollapseSpaces(input string) string {
	var s strings.Builder
	var flag bool
	for _, x := range input {
		if !unicode.IsSpace(x) {
			s.WriteRune(x)
			flag = false
		} else {
			if !flag {
				s.WriteRune(' ')
			}
			flag = true
		}
	}
	return s.String()
}
